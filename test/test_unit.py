import os
import io
import sys
import shutil
import importlib
import configparser
from copy import copy
from unittest import mock, TestCase
from contextlib import contextmanager

import pytest
import yaml
import botocore.session
from botocore.stub import Stubber

from pipe import pipe


@contextmanager
def capture_output():
    standard_out = sys.stdout
    try:
        stdout = io.StringIO()
        sys.stdout = stdout
        yield stdout
    finally:
        sys.stdout = standard_out
        sys.stdout.flush()


class CodeDeployTestCase(TestCase):

    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path
        test_dir = os.path.join(os.environ["HOME"], '.aws')
        if os.path.exists(test_dir):
            shutil.rmtree(test_dir)

    @mock.patch.dict(os.environ, {'AWS_OIDC_ROLE_ARN': '', 'BITBUCKET_STEP_OIDC_TOKEN': '',
                                  'AWS_DEFAULT_REGION': 'test-region',
                                  'AWS_ACCESS_KEY_ID': 'akiafkae', 'AWS_SECRET_ACCESS_KEY': 'secretkey',
                                  'APPLICATION_NAME': 'test-app', 'COMMAND': 'deploy', 'DEPLOYMENT_GROUP': 'test-group',
                                  'BITBUCKET_COMMIT': '4cec9001b625abac0c74a4b0177501d450894162',
                                  'BITBUCKET_BUILD_NUMBER': '100500', 'ZIP_FILE': 'test/abc.zip'})
    def test_discover_auth_method_default_credentials_only(self):
        from pipe import pipe

        code_deploy_pipe = pipe.CodeDeployPipe(schema=pipe.schema, check_for_newer_version=True)
        code_deploy_pipe.auth()
        self.assertEqual(code_deploy_pipe.auth_method, 'DEFAULT_AUTH')
        base_config_dir = f'{os.environ["HOME"]}/.aws'

        self.assertFalse(os.path.exists(f'{base_config_dir}/.aws-oidc'))

    @mock.patch.dict(os.environ, {'BITBUCKET_STEP_OIDC_TOKEN': 'token', 'AWS_OIDC_ROLE_ARN': 'account/role',
                                  'AWS_DEFAULT_REGION': 'test-region',
                                  'APPLICATION_NAME': 'test-app', 'COMMAND': 'deploy', 'DEPLOYMENT_GROUP': 'test-group',
                                  'BITBUCKET_BUILD_NUMBER': '100500', 'ZIP_FILE': 'test/abc.zip',
                                  'BITBUCKET_COMMIT': '4cec9001b625abac0c74a4b0177501d450894162'})
    def test_discover_auth_method_oidc_only(self):
        from pipe import pipe

        code_deploy_pipe = pipe.CodeDeployPipe(schema=pipe.schema, check_for_newer_version=True)
        code_deploy_pipe.auth()
        self.assertEqual(code_deploy_pipe.auth_method, 'OIDC_AUTH')
        self.assertTrue(os.path.exists(f'{os.environ["HOME"]}/.aws/.aws-oidc'))
        config = configparser.ConfigParser()
        config.read(f'{os.environ["HOME"]}/.aws/config')
        self.assertEqual(config['default'].get('role_arn'), 'account/role')
        token_file = config['default'].get('web_identity_token_file')

        with open(token_file) as f:
            self.assertEqual(f.read(), 'token')

    @mock.patch.dict(os.environ, {'BITBUCKET_STEP_OIDC_TOKEN': 'token', 'AWS_OIDC_ROLE_ARN': 'account/role',
                                  'AWS_ACCESS_KEY_ID': 'akiafkae', 'AWS_SECRET_ACCESS_KEY': 'secretkey',
                                  'AWS_DEFAULT_REGION': 'test-region',
                                  'APPLICATION_NAME': 'test-app', 'COMMAND': 'deploy', 'DEPLOYMENT_GROUP': 'test-group',
                                  'BITBUCKET_BUILD_NUMBER': '100500', 'ZIP_FILE': 'test/abc.zip',
                                  'BITBUCKET_COMMIT': '4cec9001b625abac0c74a4b0177501d450894162'})
    def test_discover_auth_method_oidc_and_default_credentials(self):
        from pipe import pipe
        self.assertTrue('AWS_ACCESS_KEY_ID' in os.environ)
        self.assertTrue('AWS_SECRET_ACCESS_KEY' in os.environ)

        code_deploy_pipe = pipe.CodeDeployPipe(schema=pipe.schema, check_for_newer_version=True)
        code_deploy_pipe.auth()

        self.assertEqual(code_deploy_pipe.auth_method, 'OIDC_AUTH')
        self.assertTrue(os.path.exists(f'{os.environ["HOME"]}/.aws/.aws-oidc'))
        config = configparser.ConfigParser()
        config.read(f'{os.environ["HOME"]}/.aws/config')
        self.assertEqual(config['default'].get('role_arn'), 'account/role')
        token_file = config['default'].get('web_identity_token_file')

        with open(token_file) as f:
            self.assertEqual(f.read(), 'token')

        self.assertFalse('AWS_ACCESS_KEY_ID' in os.environ)
        self.assertFalse('AWS_SECRET_ACCESS_KEY' in os.environ)


class SuccessTestCase(TestCase):
    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path
        test_dir = os.path.join(os.environ["HOME"], '.aws')
        if os.path.exists(test_dir):
            shutil.rmtree(test_dir)

    @mock.patch.object(pipe.CodeDeployPipe, 'get_client')
    @mock.patch.dict(
        os.environ,
        {
            'AWS_ACCESS_KEY_ID': 'akiafkae', 'AWS_SECRET_ACCESS_KEY': 'secretkey', 'AWS_DEFAULT_REGION': 'test-region',
            'BITBUCKET_BUILD_NUMBER': '100500', 'BITBUCKET_COMMIT': '4cec9001b625abac0c74a4b0177501d450894162',
            'APPLICATION_NAME': 'test-app', 'COMMAND': 'deploy', 'DEPLOYMENT_GROUP': 'test-group',
            'ZIP_FILE': 'test/abc.zip', 'WAIT': 'False'
        }
    )
    def test_success(self, mock_codedeploy):
        codedeploy = botocore.session.get_session().create_client('codedeploy')
        mock_codedeploy.return_value = codedeploy

        stubber = Stubber(codedeploy)

        stubber.add_response(
            'get_application_revision',
            {},
            expected_params={
                'applicationName': 'test-app',
                'revision': {
                    'revisionType': 'S3',
                    's3Location': {
                        'bucket': 'test-app-codedeploy-deployment',
                        'bundleType': 'zip',
                        'key': 'test-app-100500-4cec9001'
                    }
                }
            }
        )
        stubber.add_response(
            'create_deployment',
            {'deploymentId': '100500'},
            expected_params={
                'applicationName': 'test-app',
                'deploymentGroupName': 'test-group',
                'description': 'Deployed from Bitbucket Pipelines using aws-code-deploy pipe. For details follow the link https://bitbucket.org/atlassian/aws-code-deploy/addon/pipelines/home#!/results/100500',
                'fileExistsBehavior': 'DISALLOW',
                'ignoreApplicationStopFailures': False,
                'revision': {
                    'revisionType': 'S3',
                    's3Location': {
                        'bucket': 'test-app-codedeploy-deployment',
                        'bundleType': 'zip',
                        'key': 'test-app-100500-4cec9001'
                    }
                }
            }
        )
        stubber.add_response(
            'get_deployment',
            {
                'deploymentInfo': {
                    'applicationName': 'test-app',
                    'deploymentGroupName': 'test-group',
                    'deploymentId': '100500'
                }
            },
            expected_params={'deploymentId': '100500'}
        )

        current_pipe_module = importlib.import_module('pipe.pipe')

        with open(os.path.join(os.getcwd(), 'pipe.yml')) as f:
            metadata = yaml.safe_load(f.read())
        codedeploy_pipe = current_pipe_module.CodeDeployPipe(
            schema=current_pipe_module.schema,
            pipe_metadata=metadata,
            check_for_newer_version=True,
        )

        with stubber:
            with capture_output() as out:
                codedeploy_pipe.run()

        self.assertRegex(
            out.getvalue(),
            r"✔ Skip waiting for deployment to complete"
        )


class NegativeTestCase(TestCase):
    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path
        test_dir = os.path.join(os.environ["HOME"], '.aws')
        if os.path.exists(test_dir):
            shutil.rmtree(test_dir)

    def test_fail_validation_no_application_name_variable(self):
        current_pipe_module = importlib.import_module('pipe.pipe')

        with open(os.path.join(os.getcwd(), 'pipe.yml')) as f:
            metadata = yaml.safe_load(f.read())

        with capture_output() as out:
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                codedeploy_pipe = current_pipe_module.CodeDeployPipe(
                    schema=current_pipe_module.schema,
                    pipe_metadata=metadata,
                    check_for_newer_version=False,
                )
                codedeploy_pipe.run()

        self.assertEqual(pytest_wrapped_e.type, SystemExit)
        self.assertRegex(
            out.getvalue(),
            r"✖ Validation error: APPLICATION_NAME variable is required"
        )
